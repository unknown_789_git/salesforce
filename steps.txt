Steps to create a Salesforce account

Step 1:
	goto : https://www.salesforce.com/in/
Step 2:
	Click on try for free button on the right upper corner of website
Step 3:
	Fill the Sign up form, and click on "Start my free trial"
Step 4:
	A verification link will be sent to your email-id for verification
Step 5:
	And you are all set to use your account